package io.purecore.core.api.type;

public class CoreKey {

    String hash;

    public CoreKey(String hash)
    {

        this.hash = hash;

    }

    public String getHash() {
        return hash;
    }
}
