package io.purecore.core.api.exception;

public class ServerApiError extends Exception{

    public ServerApiError(String error) {

        super(error);
    }

}
