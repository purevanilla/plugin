package io.purecore.api.exception;

public class ApiException extends Exception {
    public ApiException(String msg) { super(msg); }
}
