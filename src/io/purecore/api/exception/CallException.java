package io.purecore.api.exception;

public class CallException extends Exception {
    public CallException(String msg) { super(msg); }
}

