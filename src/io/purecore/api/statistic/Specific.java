package io.purecore.api.statistic;

public class Specific {

    String specificId;
    Long value;

    Specific(String specificId, Long value){
        this.specificId=specificId;
        this.value=value;
    }
}
