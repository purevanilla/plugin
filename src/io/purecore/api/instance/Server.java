package io.purecore.api.instance;

import io.purecore.api.Core;

public class Server extends Instance {

    public Server(Core core, String uuid, String name, Type type) {
        super(core, uuid, name, type);
    }

}
